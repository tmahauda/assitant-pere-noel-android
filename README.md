# Assitant Père Noël

<div align="center">
<img width="500" height="400" src="Pere noel.jpg">
</div>

## Description du projet

Application mobile réalisée avec Android en LP MIAR à l'IUT de Nantes dans le cadre du module "Programmation Mobile" durant l'année 2018-2019 avec un groupe de deux personnes. \
Elle consiste à aider le "père noël nantais" à suivre les différentes étapes de son travail.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de deux étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Mathieu PINEAU : mathieu.pineau@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'IUT de Nantes :
- Arnaud LANOIX BRAUER – arnaud.lanoix@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Programmation Mobile" de la LP MIAR.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2018-2019 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 14/01/2019.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Android ;
- Android Studio ;
- Gradle ;
- Service Web REST/JSON avec Volley ;
- Layouts ;
- ListView, RecyclerView et Adapter(s) ;
- Activité.

## Objectifs

L'application doit :
* Permettre de récupérer une liste de prénom d’enfants en fonction de différents critères (année de naissance, sexe, etc.) ;
* Afficher la liste de prénoms récupérés dans une ListView avec une présentation adaptée qui reflètera les différents états (lettre reçue, enfant sage, cadeaux livrés, etc.) ;
* Afficher de manière détaillée un prénom et permettre de modifier son état.
