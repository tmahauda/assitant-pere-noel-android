package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.ArrayList;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.controller.ControllerChildren;
import alternance.miar.lp.iut.assistant_perenoel.model.Children;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'afficher la liste des enfants
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class PereNoel extends AppCompatActivity
{
    private ControllerChildren responseChildren;

    //Les activités
    public static final int DETAIL_CHILDREN = 1;
    public static final int SEE_GIFTS = 2;
    public static final int DETAIL_GIFT = 3;
    public static final int FILTERS_CHILDRENS = 4;
    public static final int FILTERS_GIFTS = 5;

    //Les urls
    public static final int CHILDREN_MAX = 100;
    public static final String URL_CHILDREN = "https://data.loire-atlantique.fr/api/records/1.0/search/?dataset=244400404_prenoms-enfants-nes-nantes&rows="+CHILDREN_MAX+"&facet=prenom&facet=sexe&facet=annee_naissance";
    public static final String URL_GIFT = "https://api.cdiscount.com/OpenApi/json/Search";
    public static final String KEY_GIFT = "e6639310-60c1-416f-b6e3-6e40c53e8901";//"d543d0fa-d2c8-42ef-89ab-5e58957e7280";

    //Fichier Json
    public static final String NAME_FILE_JSON = "childrens.json";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pere_noel);

        //Creation de la liste des enfants
        this.responseChildren = new ControllerChildren(this);

        //Si les données existe localement
        if(this.responseChildren.existJson()) //this.responseChildren.existJson()
        {
            //On récupère les données locales
            JSONObject data = this.responseChildren.readData();
            this.responseChildren.onResponse(data);
        }
        //Sinon on essaye de récupèrer les données sur internet
        else alternance.miar.lp.iut.assistant_perenoel.controller.Request.executeRequest(this, Request.Method.GET, PereNoel.URL_CHILDREN, null, this.responseChildren);

        //Envoi vers la vue filtrage de la liste des enfants
        Button buttonFiltersChildrens = findViewById(R.id.buttonFiltersChildrens);
        buttonFiltersChildrens.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //On récupère la liste des modeles (enfants) pour l'envoyer au filtre afin
                //d'effectuer des modifications
                Intent intent = new Intent(PereNoel.this, FiltersChildren.class);
                intent.putParcelableArrayListExtra("childrens", responseChildren.getModels());
                startActivityForResult(intent, PereNoel.FILTERS_CHILDRENS);
            }
        });

        //Enregistre au format JSON les informations de chaque enfant
        Button buttonRecordChildrens = findViewById(R.id.buttonRecordChildrens);
        buttonRecordChildrens.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                responseChildren.recordChildrens();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result)
    {
        //On met à jour un enfant de la liste
        if (resultCode == RESULT_OK && requestCode == PereNoel.DETAIL_CHILDREN)
        {
            final Children children = result.getExtras().getParcelable("children");
            this.responseChildren.update(children);
            Toast.makeText(this,"Enfant mise à jour",Toast.LENGTH_LONG ).show();
        }
        //On met à jour toute la liste des enfants
        else if (resultCode == RESULT_OK && requestCode == PereNoel.FILTERS_CHILDRENS)
        {
            final ArrayList<Model> childrens = result.getExtras().getParcelableArrayList("childrens");
            this.responseChildren.updateModels(childrens);
            Toast.makeText(this,"Enfants mises à jour",Toast.LENGTH_LONG ).show();
        }
    }
}
