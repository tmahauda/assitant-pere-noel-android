package alternance.miar.lp.iut.assistant_perenoel.controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;

import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe abstraite qui permet selon des données JSON récupérées grâce à un URL pour les enfants ou les cadeaux :
 * - De peupler la liste des modèles
 * - De peupler la liste vue selon la liste des modèles grâce à un adaptateur
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public abstract class Controller implements com.android.volley.Response.Listener<JSONObject>, Response.ErrorListener
{
    protected AppCompatActivity activity;
    protected ArrayAdapter<Model> adapter;
    protected ArrayList<Model> models;
    protected Comparator<Model> comparator;
    private ProgressDialog progressDialog;

    /**
     * Méthode qui permet de récupérer la liste des modèles
     * @return la liste des modèles
     */
    public ArrayList<Model> getModels()
    {
        return this.models;
    }

    /**
     * Méthode qui permet de modifier la liste des modèles
     * @param models les modèles à charger
     */
    public void setModels(ArrayList<Model> models)
    {
        this.models=models;
        if(this.models.isEmpty())
        {
            this.displayAlert();
        }
        else this.changeList();
    }

    public Controller(AppCompatActivity activity)
    {
        this.activity = activity;
        this.models = new ArrayList<>();
        this.progressDialog = new ProgressDialog(activity);
    }

    /**
     * Méthode qui permet de traiter la réponse renvoyer par la requete sous forme de données JSON
     * @param response qui contient les données sous forme JSON
     */
    @Override
    public void onResponse(JSONObject response)
    {
        this.models = this.populateModels(response);
        if(this.models.isEmpty())
        {
            this.displayAlert();
        }
        else this.changeList();
        this.progressDialog.dismiss();
    }

    /**
     * Méthode qui permet d'afficher une boite de dialogue "Patienter..." lors de la récupération des données
     * à partir d'internet (Volley)
     */
    public void displayProgress()
    {
        this.progressDialog.setTitle("Veuillez patienter s'il vous plait");
        this.progressDialog.setMessage("Chargement des données en cours ...");
        this.progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        this.progressDialog.show();
    }

    /**
     * Méthode qui permet d'afficher une boite de dialogue "Erreur" si les données
     * n'ont pu être récupérer depuis internet (Volley)
     * @param error le message d'erreur
     */
    @Override
    public void onErrorResponse(VolleyError error)
    {
        System.out.println(error.getMessage());
        this.progressDialog.dismiss();
        this.displayAlert();
    }

    /**
     * Méthode qui permet d'afficher une erreur lors de la récupération des données
     * et referme l'activité
     */
    private void displayAlert()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setTitle("Erreur");
        builder.setMessage("Erreur lors du chargement des données. Réessayer ultérieurement");
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                activity.finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Méthode qui permet de peupler la liste des modèles selon des données JSON récupérées par internet
     * @param response contenant les données JSON pour créer les modèles
     * @return une nouvelle liste de modèles
     */
    protected abstract ArrayList<Model> populateModels(JSONObject response);

    /**
     * Méthode qui permet de connecter la listview à un adaptateur
     * pour la peupler grâce à la liste des modèles
     */
    protected abstract void changeList();

    /**
     * Méthode qui permet de mettre à jour la liste des modèles selon
     * une liste de filtre
     * @param models la liste des modèles à mettre à jour
     */
    public void updateModels(ArrayList<Model> models)
    {
        this.adapter.clear();
        this.adapter.addAll(models);
        this.adapter.sort(this.comparator);
        this.adapter.notifyDataSetChanged();
    }

    /**
     * Méthode qui permet de modifier un modele
     * @param model le modele à modifier dans la liste des modèles
     */
    public void update(Model model)
    {
        this.models.remove(model);
        this.models.add(model);

        this.adapter.remove(model);
        this.adapter.add(model);

        this.adapter.sort(this.comparator);
        this.adapter.notifyDataSetChanged();
    }
}
