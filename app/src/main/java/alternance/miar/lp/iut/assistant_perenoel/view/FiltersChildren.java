package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.controller.Filter;
import alternance.miar.lp.iut.assistant_perenoel.controller.FilterChildren;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'afficher les filtres à appliquer pour la liste des enfants
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class FiltersChildren extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters_children);

        ArrayList<Model> models = getIntent().getExtras().getParcelableArrayList("childrens");

        //Filtre
        final Filter filter = new FilterChildren(this, models);
        filter.initializeFilters();

        //Enregistrer
        final Button buttonRecordFilters = findViewById(R.id.buttonRecordFiltersChildrens);
        buttonRecordFilters.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.putParcelableArrayListExtra("childrens", filter.getModels());
                setResult(PereNoel.RESULT_OK, intent);
                finish();
            }
        });

    }
}
