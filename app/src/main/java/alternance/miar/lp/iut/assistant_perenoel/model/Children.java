package alternance.miar.lp.iut.assistant_perenoel.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

/**
 * Classe qui permet de représenter un enfant
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class Children extends Model
{
    private ArrayList<Gift> wishedGifts;
    private ArrayList<Gift> confirmedGifts;
    private String name;
    private String sex;
    private int years;
    private boolean receivedLetter;
    private boolean sage;
    private boolean deliveredGifts;

    public Children(Parcel in)
    {
        this.wishedGifts = in.readArrayList(Gift.class.getClassLoader());
        this.confirmedGifts = in.readArrayList(Gift.class.getClassLoader());
        this.id = in.readString();
        this.name = in.readString();
        this.sex = in.readString();
        this.years = in.readInt();
        this.receivedLetter = in.readByte() != 0;
        this.sage = in.readByte() != 0;
        this.deliveredGifts = in.readByte() != 0;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeList(this.wishedGifts);
        dest.writeList(this.confirmedGifts);
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.sex);
        dest.writeInt(this.years);
        dest.writeByte((byte) (this.receivedLetter ? 1 : 0));
        dest.writeByte((byte) (this.sage ? 1 : 0));
        dest.writeByte((byte) (this.deliveredGifts ? 1 : 0));
    }

    public static final Parcelable.Creator<Children> CREATOR = new Parcelable.Creator<Children>()
    {
        public Children createFromParcel(Parcel in)
        {
            return new Children(in);
        }

        public Children[] newArray(int size)
        {
            return new Children[size];
        }
    };


    public Children(String name, String sex, int years, boolean receivedLetter, boolean sage, boolean deliveredGifts)
    {
        this.id = name;
        this.wishedGifts = new ArrayList<>();
        this.confirmedGifts = new ArrayList<>();
        this.name = name;
        this.sex = sex;
        this.years = years;
        this.receivedLetter = receivedLetter;
        this.sage = sage;
        this.deliveredGifts = deliveredGifts;
    }

    public void addWishedGift(Gift gift)
    {
        if(!this.wishedGifts.contains(gift))
        {
            this.wishedGifts.add(gift);
        }
    }

    public void removeWishedGift(Gift gift)
    {
        if(this.wishedGifts.contains(gift))
        {
            this.wishedGifts.remove(gift);
        }
    }

    public int sizeWishedGift()
    {
        return this.wishedGifts.size();
    }

    public void addConfirmedGift(Gift gift)
    {
        if(!this.confirmedGifts.contains(gift))
        {
            this.confirmedGifts.add(gift);
        }
    }

    public void removeConfirmedGift(Gift gift)
    {
        if(this.confirmedGifts.contains(gift))
        {
            this.confirmedGifts.remove(gift);
        }
    }

    public int sizeConfirmedGift()
    {
        return this.confirmedGifts.size();
    }


    public ArrayList<Gift> getWishedGifts() {
        return wishedGifts;
    }

    public void setWishedGifts(ArrayList<Gift> wishedGifts) {
        this.wishedGifts = wishedGifts;
    }

    public ArrayList<Gift> getConfirmedGifts() {
        return confirmedGifts;
    }

    public void setConfirmedGifts(ArrayList<Gift> confirmedGifts) {
        this.confirmedGifts = confirmedGifts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    public boolean isReceivedLetter() {
        return receivedLetter;
    }

    public void setReceivedLetter(boolean receivedLetter) {
        this.receivedLetter = receivedLetter;
    }

    public boolean isSage() {
        return sage;
    }

    public void setSage(boolean sage) {
        this.sage = sage;
    }

    public boolean isDeliveredGifts() {
        return deliveredGifts;
    }

    public void setDeliveredGifts(boolean deliveredGifts) {
        this.deliveredGifts = deliveredGifts;
    }

    @Override
    public String toString() {
        return "Children{" +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", years=" + years +
                ", receivedLetter=" + receivedLetter +
                ", sage=" + sage +
                ", deliveredGifts=" + deliveredGifts +
                ", sizeWishedGift=" + this.sizeWishedGift() +
                ", sizeConfirmedGift=" + this.sizeConfirmedGift() +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Children children = (Children) o;
        return Objects.equals(name, children.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name);
    }


    public static final Comparator<Model> COMPARATOR_CHILDREN_NAME = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Children child1 = (Children)model1;
            Children child2 = (Children)model2;

            return child1.getName().compareTo(child2.getName());
        }
    };

    public static final Comparator<Model> COMPARATOR_CHILDREN_SEX = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Children child1 = (Children)model1;
            Children child2 = (Children)model2;

            return child1.getSex().compareTo(child2.getSex());
        }
    };

    public static final Comparator<Model> COMPARATOR_CHILDREN_AGE = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Children child1 = (Children)model1;
            Children child2 = (Children)model2;

            if(child1.getYears() > child2.getYears())
            {
                return 1;
            }
            else if(child1.getYears() < child2.getYears())
            {
                return -1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_CHILDREN_LETTER = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Children child1 = (Children)model1;
            Children child2 = (Children)model2;

            if(child1.isReceivedLetter() && !child2.isReceivedLetter())
            {
                return -1;
            }
            else if(!child1.isReceivedLetter() && child2.isReceivedLetter())
            {
                return 1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_CHILDREN_SAGE = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Children child1 = (Children)model1;
            Children child2 = (Children)model2;

            if(child1.isSage() && !child2.isSage())
            {
                return -1;
            }
            else if(!child1.isSage() && child2.isSage())
            {
                return 1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_CHILDREN_GIFT = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Children child1 = (Children)model1;
            Children child2 = (Children)model2;

            if(child1.isDeliveredGifts() && !child2.isDeliveredGifts())
            {
                return 1;
            }
            else if(!child1.isDeliveredGifts() && child2.isDeliveredGifts())
            {
                return -1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_CHILDREN_CONFIRMED_GIFT = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Children child1 = (Children)model1;
            Children child2 = (Children)model2;

            if(child1.sizeConfirmedGift() > child2.sizeConfirmedGift())
            {
                return 1;
            }
            else if(child1.sizeConfirmedGift() < child2.sizeConfirmedGift())
            {
                return -1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_CHILDREN_DEFAULT = new Comparator<Model>()
    {
        final Comparator<Model>[] comparators = new Comparator[]{COMPARATOR_CHILDREN_GIFT, COMPARATOR_CHILDREN_LETTER,
                COMPARATOR_CHILDREN_SAGE, COMPARATOR_CHILDREN_CONFIRMED_GIFT, COMPARATOR_CHILDREN_SEX, COMPARATOR_CHILDREN_AGE, COMPARATOR_CHILDREN_NAME};

        @Override
        public int compare(Model model1, Model model2)
        {
           return COMPARATOR_MODEL(comparators).compare(model1, model2);
        }
    };
}
