package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.Collections;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.controller.ControllerGift;
import alternance.miar.lp.iut.assistant_perenoel.model.Children;
import alternance.miar.lp.iut.assistant_perenoel.model.Gift;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'afficher la liste des cadeaux souhaités d'un enfant
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class Letter extends AppCompatActivity
{
    private ControllerGift responseGift;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letter);

        //On récupère l'enfant
        final Children children = getIntent().getExtras().getParcelable("children");

        //Initialise la liste des cadeaux souhaités
        this.responseGift = new ControllerGift(this);

        if(!children.getWishedGifts().isEmpty())
        {
            ArrayList<Model> models = new ArrayList<>();

            for(Gift gift : children.getWishedGifts())
            {
                models.add(gift);
            }

            this.responseGift.setModels(models);
        }
        else
        {
            alternance.miar.lp.iut.assistant_perenoel.controller.Request.executeRequest(this, Request.Method.POST, PereNoel.URL_GIFT,
                    alternance.miar.lp.iut.assistant_perenoel.controller.Request.getParametersGifts(children),this.responseGift);
        }

        //Enregistre les cadeaux confirmés et souhaités
        final Button buttonRecordLetter = findViewById(R.id.buttonRecordLetter);
        buttonRecordLetter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                children.setConfirmedGifts(responseGift.populateConfirmedGifts());
                children.setWishedGifts(responseGift.populateWishedGifts());
                Intent intent = new Intent();
                intent.putExtra("children", children);
                setResult(DetailChildren.RESULT_OK, intent);
                finish();
            }
        });

        //Envoi vers la page de filtrage des cadeaux
        final Button buttonFilterGift = findViewById(R.id.buttonFilterGift);
        buttonFilterGift.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Letter.this, FiltersGift.class);
                intent.putParcelableArrayListExtra("gifts", responseGift.getModels());
                startActivityForResult(intent, PereNoel.FILTERS_GIFTS);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result)
    {
        //On met à jour un cadeau de la liste
        if (resultCode == RESULT_OK && requestCode == PereNoel.DETAIL_GIFT)
        {
            final Gift gift = result.getExtras().getParcelable("gift");
            this.responseGift.update(gift);
            Toast.makeText(this,"Cadeau mise à jour",Toast.LENGTH_LONG ).show();
        }
        //On met à jour toute la liste des cadeaux
        else if (resultCode == RESULT_OK && requestCode == PereNoel.FILTERS_GIFTS)
        {
            final ArrayList<Model> gifts = result.getExtras().getParcelableArrayList("gifts");
            this.responseGift.updateModels(gifts);
            Toast.makeText(this,"Cadeaux mises à jour",Toast.LENGTH_LONG ).show();
        }
    }
}
