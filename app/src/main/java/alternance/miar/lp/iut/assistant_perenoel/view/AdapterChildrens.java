package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Children;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe permettant de redéfinir la liste des enfants afin d'afficher les informations suivantes pour chaque enfant :
 * - S'il est sage. Si oui, un logo ange de couleur verte est affiché. Sinon un logo diable de couleur rouge est affiché.
 * - Si c'est un garçon ou une fille. Si c'est une fille dans ce cas le nom est surligné en rose. Sinon c'est un garçon le nom est surligné en bleu.
 * - S'il a envoyé la lettre ou non. Si oui, dans ce cas la ligne est visible. Sinon la ligne est grisée légèrement.
 * - Le nombre de cadeaux confirmés pour cet enfant.
 * - Si cadeaux reçu, image terminé en premier plan de la ligne
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class AdapterChildrens extends ArrayAdapter<Model>
{

    public AdapterChildrens(AppCompatActivity activity, ArrayList<Model> childrens)
    {
        super(activity, android.R.layout.simple_list_item_1, childrens);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_row_layout_children, parent, false);

        final Children children = (Children) this.getItem(position);
        final TextView textTitle = rowView.findViewById(R.id.textListNameChildren);
        final TextView textSubtitle = rowView.findViewById(R.id.textListYearsChildren);
        final ImageView imageChildren = rowView.findViewById(R.id.imageListChildren);
        final ProgressBar progressBarChildren = rowView.findViewById(R.id.progressBarListChildren);
        final TextView minProgressBarChildren = rowView.findViewById(R.id.textMinProgressBar);
        final TextView maxProgressBarChildren = rowView.findViewById(R.id.textMaxProgressBar);
        final TextView textProgressBarChildren = rowView.findViewById(R.id.textProgressBar);
        final ImageView imageFinish = rowView.findViewById(R.id.imageFinish);

        int maxConfirmedGifts = children.sizeConfirmedGift();
        minProgressBarChildren.setText(String.valueOf(0));
        maxProgressBarChildren.setText(String.valueOf(10));
        textProgressBarChildren.setText("Nombre de cadeaux : " + String.valueOf(maxConfirmedGifts));

        progressBarChildren.setMax(10);
        progressBarChildren.setProgress(maxConfirmedGifts);

        imageChildren.getLayoutParams().height = 100;
        imageChildren.getLayoutParams().width = 100;

        if(children.isSage()) imageChildren.setImageDrawable(getContext().getDrawable(R.drawable.angel));
        else imageChildren.setImageDrawable(getContext().getDrawable(R.drawable.evil));

        if(children.getSex().equals("FILLE"))
        {
            this.changeTextView(textTitle, Color.rgb(255,20,147), children.getName());
            this.changeTextView(textSubtitle, Color.rgb(255,20,147), String.valueOf(children.getYears()) + " ans");
        }
        else
        {
            this.changeTextView(textTitle, Color.BLUE, children.getName());
            this.changeTextView(textSubtitle, Color.BLUE, String.valueOf(children.getYears()) + " ans");
        }

        if(children.isDeliveredGifts()) imageFinish.setVisibility(View.VISIBLE);

        if(!children.isReceivedLetter())
        {
            imageChildren.setAlpha(0.3f);
            textTitle.setAlpha(0.3f);
            textSubtitle.setAlpha(0.3f);
            imageChildren.setAlpha(0.3f);
            progressBarChildren.setAlpha(0.3f);
            minProgressBarChildren.setAlpha(0.3f);
            maxProgressBarChildren.setAlpha(0.3f);
            textProgressBarChildren.setAlpha(0.3f);
        }

        return rowView;
    }

    private void changeTextView(TextView textView, int color, String text)
    {
        textView.setTextColor(color);
        textView.setText(text);
    }
}
