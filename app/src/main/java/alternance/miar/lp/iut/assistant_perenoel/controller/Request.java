package alternance.miar.lp.iut.assistant_perenoel.controller;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import alternance.miar.lp.iut.assistant_perenoel.model.Children;
import alternance.miar.lp.iut.assistant_perenoel.view.PereNoel;

/**
 * Classe qui permet d'effectuer une requête pour récupérer des données JSON
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class Request
{
    public static void executeRequest(AppCompatActivity activity, int method, String url, JSONObject parameters, Controller controller)
    {
        final RequestQueue requestQueue = Volley.newRequestQueue(activity);
        final JsonObjectRequest requestChildren = new JsonObjectRequest(method, url, parameters,
                controller, controller
        );
        requestQueue.add(requestChildren);
        displayProgress(controller);
    }

    private static void displayProgress(Controller controller)
    {
        controller.displayProgress();
    }

    public static JSONObject getParametersGifts(Children children)
    {
        JSONObject parameters = new JSONObject();

        try
        {
            JSONObject page = new JSONObject();
            page.put("ItemsPerPage", "10");
            page.put("PageNumber", String.valueOf(getRandomNumberInRange(0,5)));

            JSONObject filters = new JSONObject();
            filters.put("Price", new JSONObject());
            filters.put("Navigation", "toys");
            filters.put("IncludeMarketPlace", "false");
            filters.put("Condition", "all");

            JSONObject search = new JSONObject();
            search.put("Keyword", "jouet");
            search.put("SortBy", "relevance");
            search.put("Pagination", page);
            search.put("SortItemListener", filters);

            parameters.put("ApiKey", PereNoel.KEY_GIFT);
            parameters.put("SearchRequest", search);
        }
        catch (JSONException e)
        {
            System.out.println(e.getMessage());
        }

        return parameters;
    }

    private static int getRandomNumberInRange(int min, int max)
    {
        if (min < max)
        {
            Random r = new Random();
            return r.nextInt((max - min) + 1) + min;
        }
        else return 0;
    }
}
