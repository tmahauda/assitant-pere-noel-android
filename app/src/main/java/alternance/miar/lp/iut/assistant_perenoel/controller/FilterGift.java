package alternance.miar.lp.iut.assistant_perenoel.controller;

import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Gift;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'appliquer des filtres pour la liste des cadeaux
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class FilterGift extends Filter
{
    public FilterGift(AppCompatActivity activity, ArrayList<Model> models)
    {
        super(activity, models);
    }

    @Override
    protected void applyFiltersSpinners()
    {

    }

    @Override
    protected void applyFiltersEditText()
    {
        for(EditText editTextMin : this.editTexts.keySet())
        {
            for(EditText editTextMax : this.editTexts.get(editTextMin).keySet())
            {
                String attribut = this.editTexts.get(editTextMin).get(editTextMax);
                double max = Double.valueOf(editTextMax.getText().toString());
                double min = Double.valueOf(editTextMin.getText().toString());

                this.filtersModels(attribut, min, max);
            }
        }
    }

    @Override
    protected void applyFiltersToggleButtons()
    {
        for(MultiStateToggleButton toggleButton : this.toggleButtons.keySet())
        {
            String attribut = this.toggleButtons.get(toggleButton);

            if(toggleButton.getStates()[0])
            {
                this.<Boolean>filtersModels(attribut, true);
            }
            else if(toggleButton.getStates()[1])
            {
                this.<Boolean>filtersModels(attribut, false);
            }
        }
    }

    @Override
    protected void initializeFiltersToggleButtons()
    {
        MultiStateToggleButton multiStateToggleButton;
        List<String> filters;

        filters = Arrays.asList("Oui", "Non");

        multiStateToggleButton = (MultiStateToggleButton)this.activity.findViewById(R.id.toggleButtonFilterAvailableGift);
        this.populateToggleButton(multiStateToggleButton, filters);
        this.toggleButtons.put(multiStateToggleButton,"isAvailable");

        multiStateToggleButton = (MultiStateToggleButton)this.activity.findViewById(R.id.toggleButtonFilterConfirmedGift);
        this.populateToggleButton(multiStateToggleButton, filters);
        this.toggleButtons.put(multiStateToggleButton,"isConfirmed");
    }

    @Override
    protected void initializeFiltersEditText()
    {
        final EditText priceMinGift = this.activity.findViewById(R.id.editTextFilterPriceMinGift);
        final EditText priceMaxGift = this.activity.findViewById(R.id.editTextFilterPriceMaxGift);

        Gift giftPriceMin = (Gift)Collections.min(this.models, Gift.COMPARATOR_GIFT_PRICE);
        Gift giftPriceMax = (Gift)Collections.max(this.models, Gift.COMPARATOR_GIFT_PRICE);

        double priceMin = giftPriceMin.getPrice();
        double priceMax = giftPriceMax.getPrice();

        priceMinGift.setText(String.valueOf(priceMin));
        priceMaxGift.setText(String.valueOf(priceMax));

        Map<EditText, String> editTextString = new HashMap<>();
        editTextString.put(priceMaxGift, "getPrice");

        this.editTexts.put(priceMinGift, editTextString);
    }

    @Override
    protected void initializeFiltersSpinners()
    {

    }
}
