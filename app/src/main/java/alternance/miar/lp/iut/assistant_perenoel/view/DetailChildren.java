package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.util.ArrayList;
import java.util.Arrays;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Children;
import alternance.miar.lp.iut.assistant_perenoel.model.Gift;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'afficher les attributs d'un enfant :
 * - Son nom
 * - Son sexe
 * - Son age
 * - Si il a envoyé la lettre
 * - Si il a été sage
 * - Si il a recu les cadeaux
 * - Sa liste de cadeaux souhaités
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class DetailChildren extends AppCompatActivity
{
    private Children children;
    private ArrayAdapter<Model> adapterGifts;

    private void changeToggleButton(MultiStateToggleButton button, boolean[] selected)
    {
        button.setElements(Arrays.asList("Oui", "Non"), selected);
        button.setColors(this.getColor(R.color.button_primary_color), this.getColor(R.color.button_secondary_color));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_children);

        //On récupère l'enfant sélectionné
        this.children = getIntent().getExtras().getParcelable("children");

        //On affiche les attributs de l'enfant

        //Son nom
        final TextView nameChildren = findViewById(R.id.textNameChildren);
        nameChildren.setText(children.getName());

        //Son sexe
        final TextView sexChildren = findViewById(R.id.textSexChildren);
        sexChildren.setText(children.getSex());

        //Son age
        final TextView ageChildren = findViewById(R.id.textAgeChildren);
        ageChildren.setText(String.valueOf(children.getYears()) + " ANS");

        //Lettre recu ou non
        final MultiStateToggleButton receivedLetterChildren = findViewById(R.id.toggleReceivedLetterChildren);
        this.changeToggleButton(receivedLetterChildren, new boolean[]{children.isReceivedLetter(), !children.isReceivedLetter()});

        //Sage ou non
        final MultiStateToggleButton sageLetterChildren = findViewById(R.id.toggleSageLetterChildren);
        this.changeToggleButton(sageLetterChildren, new boolean[]{children.isSage(), !children.isSage()});

        //Cadeaux délivrés ou non
        final MultiStateToggleButton deliveredGiftChildren = findViewById(R.id.toggleDeliveredGiftChildren);
        this.changeToggleButton(deliveredGiftChildren, new boolean[]{children.isDeliveredGifts(), !children.isDeliveredGifts()});

        //Affiche la lettre (la liste des cadeaux souhaités) de l'enfant
        final Button letterChildren = findViewById(R.id.buttonLetterChildren);
        letterChildren.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(DetailChildren.this, Letter.class);
                intent.putExtra("children", children);
                startActivityForResult(intent, PereNoel.SEE_GIFTS);
            }
        });

        //Enregistre les informations relatives à l'enfant
        final Button confirmedChildren = findViewById(R.id.buttonConfirmedChildren);
        confirmedChildren.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                children.setReceivedLetter(receivedLetterChildren.getStates()[0]);
                children.setSage(sageLetterChildren.getStates()[0]);
                children.setDeliveredGifts(deliveredGiftChildren.getStates()[0]);
                intent.putExtra("children", children);

                setResult(PereNoel.RESULT_OK, intent);
                finish();
            }
        });

        //On associe un adaptateur à la liste view pour les cadeaux confirmés
        final ListView listGiftsChildren = findViewById(R.id.listGiftsChildren);
        this.adapterGifts = new AdapterGifts(this, new ArrayList<Model>(children.getConfirmedGifts()));
        listGiftsChildren.setAdapter(this.adapterGifts);
        this.adapterGifts.notifyDataSetChanged();

        //Affiche le détail du cadeau sélectionné
        listGiftsChildren.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Gift gift = (Gift)adapterGifts.getItem(position);
                Intent intent = new Intent(DetailChildren.this, DetailGift.class);
                intent.putExtra("gift", gift);
                startActivityForResult(intent, PereNoel.DETAIL_GIFT);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result)
    {
        //Met à jour la liste des cadeaux confirmés
        if(resultCode == RESULT_OK && requestCode == PereNoel.SEE_GIFTS)
        {
            this.children = result.getExtras().getParcelable("children");
            this.adapterGifts.clear();
            this.adapterGifts.addAll(this.children.getConfirmedGifts());
            this.adapterGifts.sort(Gift.COMPARATOR_GIFT_DEFAULT);
            this.adapterGifts.notifyDataSetChanged();
        }
        //Supprime un cadeau dans la liste des confirmations si changement d'avis
        else if(resultCode == RESULT_OK && requestCode == PereNoel.DETAIL_GIFT)
        {
            final Gift gift = result.getExtras().getParcelable("gift");
            if(!gift.isConfirmed())
            {
                this.children.removeWishedGift(gift);
                this.children.addWishedGift(gift);
                this.children.removeConfirmedGift(gift);
                this.adapterGifts.remove(gift);
                this.adapterGifts.sort(Gift.COMPARATOR_GIFT_DEFAULT);
                this.adapterGifts.notifyDataSetChanged();
            }
        }
    }
}
