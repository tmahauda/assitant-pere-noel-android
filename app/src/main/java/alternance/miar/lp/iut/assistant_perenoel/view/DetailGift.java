package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Gift;

/**
 * Classe qui permet d'afficher les attributs d'un cadeau :
 * - L'image
 * - La marque
 * - Le nom
 * - Le prix
 * - Si disponible
 * - Si confirmé par le père Noël
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class DetailGift extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gift);

        //On récupère le cadeau sélectionné
        final Gift gift = getIntent().getExtras().getParcelable("gift");

        //On affiche les attributs du cadeau

        //L'image
        final ImageView imageGift = findViewById(R.id.imageGift);
        Picasso.get().load(gift.getUrlImage()).into(imageGift);
        imageGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(DetailGift.this, ImageGift.class);
                intent.putExtra("url", gift.getUrlImage());
                startActivity(intent);
            }
        });

        //Sa marque
        final TextView textBrandGift = findViewById(R.id.textBrandGift);
        textBrandGift.setText(gift.getBrand());

        //Son nom
        final TextView textNameGift = findViewById(R.id.textNameGift);
        textNameGift.setText(gift.getName());

        //Son prix
        final TextView textPriceGift = findViewById(R.id.textPriceGift);
        textPriceGift.setText(String.valueOf(gift.getPrice()) + "€");

        //Sa disponibilité
        final TextView textAvailableGift = findViewById(R.id.textAvailableGift);
        textAvailableGift.setText(gift.isAvailable() ? "Disponible" : "Non disponible");

        //Sa description
        final TextView textDescriptionGift = findViewById(R.id.textDescriptionGift);
        textDescriptionGift.setText(gift.getDescription());

        //Confirmé ou non par le père Noël
        final ToggleButton toggleConfirmedGift = findViewById(R.id.toggleConfirmedGift);
        toggleConfirmedGift.setChecked(gift.isConfirmed());

        //Enregistre les informations relative au cadeau (si confirmé ou non)
        final Button buttonRecordGift = findViewById(R.id.buttonRecordGift);
        buttonRecordGift.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                gift.setConfirmed(toggleConfirmedGift.isChecked());
                intent.putExtra("gift", gift);
                setResult(Letter.RESULT_OK, intent);
                finish();
            }
        });

    }
}
