package alternance.miar.lp.iut.assistant_perenoel.controller;

import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'appliquer des filtres pour une liste de modèle
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public abstract class Filter
{
    protected AppCompatActivity activity;
    protected ArrayList<Model> models;
    protected Map<MultiStateToggleButton,String> toggleButtons;
    protected Map<Spinner,Map<Spinner, String>> spinners;
    protected Map<EditText,Map<EditText, String>> editTexts;

    public Filter(AppCompatActivity activity, ArrayList<Model> models)
    {
        this.activity = activity;
        this.models = models;
        this.toggleButtons = new HashMap<>();
        this.spinners = new HashMap<>();
        this.editTexts = new HashMap<>();
    }

    /**
     * Méthode qui permet de récupérer la liste des modèles triées
     * @return la liste des modèles triés
     */
    public ArrayList<Model> getModels()
    {
        this.applyFilters();
        return this.models;
    }

    /**
     * Méthode qui permet d'appliquer les filtres grâce aux différents éléments:
     * - A partir des toggle buttons
     * - A partir des spinners
     * - A partir des edit text
     */
    protected void applyFilters()
    {
        this.applyFiltersToggleButtons();
        this.applyFiltersSpinners();
        this.applyFiltersEditText();
    }

    /**
     * Méthode qui permet d'appliquer des filtres grâce aux spinners
     */
    protected abstract void applyFiltersSpinners();

    /**
     * Méthode qui permet d'appliquer des filtres grâce aux toggles buttons
     */
    protected abstract void applyFiltersToggleButtons();

    /**
     * Méthode qui permet d'appliquer des filtres grâce aux edits texts
     */
    protected abstract void applyFiltersEditText();

    /**
     * Méthode qui permet de filtrer la liste des modèles au fonction d'un critère sur
     * un attribut
     * @param attribut l'attribut choisi
     * @param critere à appliquer sur l'attribut
     * @param <T> le type d'attribut (String, Boolean, ...)
     */
    protected <T> void filtersModels(String attribut, T critere)
    {
        for(Iterator<Model> iterator = this.models.iterator(); iterator.hasNext(); )
        {
            Model model = iterator.next();

            if(!model.<T>get(attribut).equals(critere))
            {
                iterator.remove();
            }
        }
    }

    /**
     * Méthode qui permet de filtrer la liste des modèles au fonction d'un critère de type Integer sur
     * un attribut qui doit se situer entre le min et la max
     * @param attribut l'attribut choisi
     * @param min la valeur min
     * @param min la valeur max
     */
    protected void filtersModels(String attribut, Integer min, Integer max)
    {
        for(Iterator<Model> iterator = this.models.iterator(); iterator.hasNext(); )
        {
            Model model = iterator.next();

            if(!(model.<Integer>get(attribut) >= min && model.<Integer>get(attribut) <= max))
            {
                iterator.remove();
            }
        }
    }

    /**
     * Méthode qui permet de filtrer la liste des modèles au fonction d'un critère de type Double sur
     * un attribut qui doit se situer entre le min et la max
     * @param attribut l'attribut choisi
     * @param min la valeur min
     * @param min la valeur max
     */
    protected void filtersModels(String attribut, Double min, Double max)
    {
        for(Iterator<Model> iterator = this.models.iterator(); iterator.hasNext(); )
        {
            Model model = iterator.next();

            if(!(model.<Double>get(attribut) >= min && model.<Double>get(attribut) <= max))
            {
                iterator.remove();
            }
        }
    }

    /**
     * Méthode qui permet d'initialiser les différents filtres
     * - A partir des toggle buttons
     * - A partir des spinners
     * - A partir des edit text
     */
    public void initializeFilters()
    {
        this.initializeFiltersToggleButtons();
        this.initializeFiltersSpinners();
        this.initializeFiltersEditText();
    }

    /**
     * Méthode qui permet d'initialiser les toggles buttons
     */
    protected abstract void initializeFiltersToggleButtons();

    /**
     * Méthode qui permet d'initialiser les spinners
     */
    protected abstract void initializeFiltersSpinners();

    /**
     * Méthode qui permet d'initialiser les edits texts
     */
    protected abstract void initializeFiltersEditText();

    /**
     * Méthode qui permet d'initialiser la liste des int de min à max des spinners avec un pas de 1 entre chaque élement
     * @param comparator permettant de récupérer le min et le max d'un modèle
     * @param attribut pour récupérer le int choisi
     * @return la liste initialisé
     */
    protected List<String> getListSpinner(final Comparator<Model> comparator, final String attribut)
    {
        Model modelMin = Collections.min(this.models, comparator);
        Model modelMax = Collections.max(this.models, comparator);

        Integer min = modelMin.<Integer>get(attribut);
        Integer max = modelMax.<Integer>get(attribut);

        List<String> listSpinner = new ArrayList<>();

        for(int i=min; i<=max; i++)
        {
            listSpinner.add(String.valueOf(i));
        }

        return listSpinner;
    }

    /**
     * Méthode qui permet de peupler les spinners grâce à une liste
     * @see Filter#getListSpinner(Comparator, String)
     * @param spinner le spinner à peupler
     * @param list la liste des élements
     * @param selection pour sélectionner les élements ou non
     */
    protected void populateSpinner(final Spinner spinner, final List<String> list, final int selection)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.activity, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(selection);
    }

    /**
     * Méthode qui permet de peupler les toggles buttons grâce à une liste
     * @param button le toogle button à peupler
     * @param list la liste des élementss
     */
    protected void populateToggleButton(final MultiStateToggleButton button, final List<String> list)
    {
        button.setOnValueChangedListener(new ToggleButton.OnValueChangedListener()
        {
            @Override
            public void onValueChanged(int position)
            {
                int cpt = 0;

                for(boolean state : button.getStates())
                {
                    if(state) cpt++;

                    if(cpt>1)
                    {
                        boolean[] states = button.getStates();
                        if(position==1) states[0] = false;
                        else states[1] = false;
                        button.setStates(states);
                    }
                }
            }
        });
        button.enableMultipleChoice(true);
        button.setElements(list, new boolean[]{false, false});
        button.setColors(this.activity.getColor(R.color.button_primary_color), this.activity.getColor(R.color.button_secondary_color));
    }
}
