package alternance.miar.lp.iut.assistant_perenoel.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Gift;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;
import alternance.miar.lp.iut.assistant_perenoel.view.AdapterGifts;
import alternance.miar.lp.iut.assistant_perenoel.view.DetailGift;
import alternance.miar.lp.iut.assistant_perenoel.view.PereNoel;

/**
 * Classe qui maintient la liste des cadeaux
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class ControllerGift extends Controller
{
    public ControllerGift(AppCompatActivity activity)
    {
        super(activity);
        this.comparator = Gift.COMPARATOR_GIFT_DEFAULT;
    }

    @Override
    protected ArrayList<Model> populateModels(JSONObject response)
    {
        ArrayList<Model> models = new ArrayList<>();

            try
            {
                JSONArray products = response.getJSONArray("Products");
                Random random = new Random();

                for (int i = 0; i < products.length(); i++)
                {
                    JSONObject product = products.getJSONObject(i);
                    String id = product.has("Id") ? product.getString("Id") : "";
                    String brand = product.has("Brand") ? product.getString("Brand") : "";
                    String name = product.has("Name") ? product.getString("Name") : "";
                    String description = product.has("Description") ? product.getString("Description") : "";
                    String urlImage = product.has("MainImageUrl") ? product.getString("MainImageUrl") : "";

                    JSONObject offer = product.has("BestOffer") ? product.getJSONObject("BestOffer") : new JSONObject();
                    double price = offer.has("SalePrice") ? offer.getDouble("SalePrice") : 0;
                    boolean available = offer.has("Available") ? offer.getBoolean("Available") : random.nextBoolean();
                    boolean confirmed = offer.has("Confirmed") ? offer.getBoolean("Confirmed") : false;

                    Gift gift = new Gift(id, brand, name, description, urlImage, price, available, confirmed);
                    models.add(gift);
                }
            }
            catch (JSONException e)
            {
                System.out.println(e.getMessage());
            }

            return models;
    }

    /**
     * Méthode qui permet de récupérer la liste des cadeaux souhaités de l'enfant
     * @return la liste des cadeaux souhaités
     */
    public ArrayList<Gift> populateWishedGifts()
    {
        ArrayList<Gift> wishedGifts = new ArrayList<>();

        for(Model model : this.models)
        {
            wishedGifts.add((Gift)model);
        }

        return wishedGifts;
    }

    /**
     * Méthode qui permet de récupérer la liste des cadeaux confirmés de l'enfant
     * @return la liste des cadeaux confirmés
     */
    public ArrayList<Gift> populateConfirmedGifts()
    {
        ArrayList<Gift> confirmedGifts = new ArrayList<>();

        for(Model model : this.models)
        {
            Gift gift = (Gift)model;

            if(gift.isConfirmed())
            {
                confirmedGifts.add(gift);
            }
        }

        return confirmedGifts;
    }

    @Override
    protected void changeList()
    {
        final ListView listGifts = this.activity.findViewById(R.id.listGifts);
        this.adapter = new AdapterGifts(this.activity, new ArrayList<>(this.models));
        this.adapter.sort(this.comparator);

        listGifts.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Gift gift = (Gift)adapter.getItem(position);
                Intent intent = new Intent(activity, DetailGift.class);
                intent.putExtra("gift", gift);
                activity.startActivityForResult(intent, PereNoel.DETAIL_GIFT);
            }
        });
        listGifts.setAdapter(this.adapter);

        this.adapter.notifyDataSetChanged();
    }
}
