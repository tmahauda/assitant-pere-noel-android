package alternance.miar.lp.iut.assistant_perenoel.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import alternance.miar.lp.iut.assistant_perenoel.R;

/**
 * Classe qui permet d'afficher sur toute la page le cadeau
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class ImageGift extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gift);

        String url = getIntent().getExtras().getString("url");
        final ImageView imageBigGift = findViewById(R.id.imageBigGift);
        Picasso.get().load(url).into(imageBigGift);

        final Button buttonReturn = findViewById(R.id.buttonReturn);
        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
