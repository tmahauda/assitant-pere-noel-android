package alternance.miar.lp.iut.assistant_perenoel.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Children;
import alternance.miar.lp.iut.assistant_perenoel.model.Gift;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;
import alternance.miar.lp.iut.assistant_perenoel.view.AdapterChildrens;
import alternance.miar.lp.iut.assistant_perenoel.view.DetailChildren;
import alternance.miar.lp.iut.assistant_perenoel.view.PereNoel;

/**
 * Classe qui maintient la liste des enfants
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */

public class ControllerChildren extends Controller
{
    public ControllerChildren(AppCompatActivity activity)
    {
        super(activity);
        this.comparator = Children.COMPARATOR_CHILDREN_DEFAULT;
    }

    @Override
    protected ArrayList<Model> populateModels(JSONObject response)
    {
        ArrayList<Model> models = new ArrayList<>();

            try
            {
                Random random = new Random();
                JSONArray records = response.has("records") ? response.getJSONArray("records") : new JSONArray();

                for(int i=0; i<records.length(); i++)
                {
                    JSONObject record = records.getJSONObject(i);
                    JSONObject fields = record.has("fields") ? record.getJSONObject("fields") : new JSONObject();
                    String name = fields.has("prenom") ? fields.getString("prenom") : "";
                    String sex = fields.has("sexe") ? fields.getString("sexe") : "";
                    int years = fields.has("annee_naissance") ? Calendar.getInstance().get(Calendar.YEAR) - fields.getInt("annee_naissance") : 0;
                    boolean receivedLetter = fields.has("received_letter") ? fields.getBoolean("received_letter") : random.nextBoolean();
                    boolean sage = fields.has("sage") ? fields.getBoolean("sage") : random.nextBoolean();
                    boolean deliveredGift = fields.has("delivered_gift") ? fields.getBoolean("delivered_gift") : false;

                    Children children = new Children(name, sex, years, receivedLetter, sage, deliveredGift);

                    if(fields.has("wished_gifts"))
                    {
                        ArrayList<Gift> wishedGifts = new ArrayList<>();
                        ControllerGift controllerGift = new ControllerGift(this.activity);

                        ArrayList<Model> gifts = controllerGift.populateModels(fields.getJSONObject("wished_gifts"));

                        for(Model model : gifts)
                        {
                            wishedGifts.add((Gift)model);
                        }

                        children.setWishedGifts(wishedGifts);
                    }

                    if(fields.has("confirmed_gifts"))
                    {
                        ArrayList<Gift> confirmedGifts = new ArrayList<>();
                        ControllerGift controllerGift = new ControllerGift(this.activity);

                        ArrayList<Model> gifts = controllerGift.populateModels(fields.getJSONObject("confirmed_gifts"));

                        for(Model model : gifts)
                        {
                            confirmedGifts.add((Gift)model);
                        }

                        children.setConfirmedGifts(confirmedGifts);
                    }

                    if(!models.contains(children))
                    {
                        models.add(children);
                    }
                }
            }
            catch (JSONException e)
            {
                System.out.println(e.getMessage());
            }

            return models;
    }

    @Override
    protected void changeList()
    {
        final ListView listChildrens = this.activity.findViewById(R.id.listChildrens);
        this.adapter = new AdapterChildrens(this.activity, new ArrayList<>(this.models));
        this.adapter.sort(this.comparator);

        listChildrens.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Children children = (Children) adapter.getItem(position);
                Intent intent = new Intent(activity, DetailChildren.class);
                intent.putExtra("children", children);
                activity.startActivityForResult(intent, PereNoel.DETAIL_CHILDREN);
            }
        });
        listChildrens.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
    }

    /**
     * Méthode qui permet de vérifier si un fichier Json existe
     * @return vrai ou faux
     */
    public boolean existJson()
    {
        return new File(this.activity.getFilesDir().getAbsolutePath(), PereNoel.NAME_FILE_JSON).exists();
    }

    /**
     * Méthode qui permet de lire des données Json à partir d'un fichier
     * @return un objet Json
     */
    public JSONObject readData()
    {
        if(this.existJson())
        {
            try
            {
                File file = new File(this.activity.getFilesDir().getAbsolutePath(), PereNoel.NAME_FILE_JSON);
                InputStream is = new FileInputStream(file);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                String json = new String(buffer, "UTF-8");

                return new JSONObject(json);
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
                return new JSONObject();
            }
            catch (JSONException e)
            {
                System.out.println(e.getMessage());
                return new JSONObject();
            }
        }
        else return new JSONObject();
    }

    /**
     * Méthode qui permet d'écrire des données Json dans un fichier
     * @param saveData les données Json à enregistrer
     * @return vrai si écriture total des données dans un fichier. Sinon faux
     */
    public boolean writeData(JSONObject saveData)
    {
        try
        {
            if(!this.existJson())
            {
                new File(this.activity.getFilesDir(),PereNoel.NAME_FILE_JSON).createNewFile();
            }

            FileOutputStream fos = this.activity.openFileOutput(PereNoel.NAME_FILE_JSON, this.activity.MODE_PRIVATE);
            fos.write(saveData.toString().getBytes());
            fos.close();
            return true;
        }
        catch (FileNotFoundException e)
        {
            return false;
        }
        catch (IOException e)
        {
            return false;
        }
    }

    /**
     * Méthode qui permet d'enregistrer les données de chaque enfant
     */
    public void recordChildrens()
    {
        if (this.existJson())
        {
            new File(this.activity.getFilesDir().getAbsolutePath(), PereNoel.NAME_FILE_JSON).delete();
        }

        try
        {

            JSONObject saveData = new JSONObject();
            JSONArray saveArrayChildren = new JSONArray();

            for (Model model : this.models)
            {
                Children children = (Children) model;
                JSONObject saveObjectChildren = new JSONObject();
                JSONObject fieldsChildren = new JSONObject();

                JSONObject saveWishGifts = new JSONObject();
                JSONArray saveProductsArrayWishGifts = new JSONArray();
                JSONObject saveProductsObjectWishGifts = new JSONObject();

                JSONObject saveConfirmedGifts = new JSONObject();
                JSONArray saveProductsArrayConfirmedGifts = new JSONArray();
                JSONObject saveProductsObjectConfirmedGifts = new JSONObject();

                    // Données de l'enfant
                    saveObjectChildren.put("prenom", children.getName());
                    saveObjectChildren.put("sexe", children.getSex());
                    saveObjectChildren.put("annee_naissance", Calendar.getInstance().get(Calendar.YEAR) - children.getYears());
                    saveObjectChildren.put("sage", children.isSage());
                    saveObjectChildren.put("received_letter", children.isReceivedLetter());
                    saveObjectChildren.put("delivered_gift", children.isDeliveredGifts());

                    // Concernant les cadeaux souhaités
                    for (Gift gift : children.getWishedGifts())
                    {
                        saveWishGifts.put("Id", gift.getId());
                        saveWishGifts.put("Brand", gift.getBrand());
                        saveWishGifts.put("Name", gift.getName());
                        saveWishGifts.put("Description", gift.getDescription());
                        saveWishGifts.put("MainImageUrl", gift.getUrlImage());
                        saveWishGifts.put("BestOffer", new JSONObject().put("SalePrice", gift.getPrice()));
                        saveWishGifts.put("Available", gift.isAvailable());
                        saveWishGifts.put("Confirmed", gift.isConfirmed());

                        saveProductsArrayWishGifts.put(saveWishGifts);
                    }
                    saveProductsObjectWishGifts.put("Products", saveProductsArrayWishGifts);
                    saveObjectChildren.put("wished_gifts", saveProductsObjectWishGifts);

                    // Concernant les cadeaux confirmés
                    for (Gift gift : children.getConfirmedGifts())
                    {
                        saveConfirmedGifts.put("Id", gift.getId());
                        saveConfirmedGifts.put("Brand", gift.getBrand());
                        saveConfirmedGifts.put("Name", gift.getName());
                        saveConfirmedGifts.put("Description", gift.getDescription());
                        saveConfirmedGifts.put("MainImageUrl", gift.getUrlImage());
                        saveConfirmedGifts.put("BestOffer", new JSONObject().put("SalePrice", gift.getPrice()));
                        saveConfirmedGifts.put("IsAvailable", gift.isAvailable());
                        saveConfirmedGifts.put("Confirmed", gift.isConfirmed());

                        System.out.println(saveConfirmedGifts);
                        saveProductsArrayConfirmedGifts.put(saveConfirmedGifts);
                    }
                    saveProductsObjectConfirmedGifts.put("Products", saveProductsArrayConfirmedGifts);
                    saveObjectChildren.put("confirmed_gifts", saveProductsObjectConfirmedGifts);

                    fieldsChildren.put("fields", saveObjectChildren);

                    saveArrayChildren.put(fieldsChildren);
                }

            saveData.put("records", saveArrayChildren);
            this.writeData(saveData);

            Toast.makeText(this.activity,"Enregistrement terminé",Toast.LENGTH_LONG ).show();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}
