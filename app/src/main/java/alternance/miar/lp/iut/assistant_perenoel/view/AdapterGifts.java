package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Gift;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe permettant de redéfinir la liste des cadeaux afin d'afficher les informations suivantes pour chaque cadeau :
 * - L'image
 * - La marque
 * - Le nom
 * - Le prix
 * - Si cadeau confirmé, image confirmé en premier plan de la ligne
 * - Si cadeau indisponible, ligne en grisé
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class AdapterGifts extends ArrayAdapter<Model>
{

    public AdapterGifts(AppCompatActivity activity, ArrayList<Model> gifts)
    {
        super(activity, android.R.layout.simple_list_item_1, gifts);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_row_layout_gift, parent, false);

        final Gift gift = (Gift)this.getItem(position);

        final ImageView imageGift = rowView.findViewById(R.id.imageListGift);
        Picasso.get().load(gift.getUrlImage()).resize(200,200).into(imageGift);

        final TextView textBrandGift = rowView.findViewById(R.id.textListBrandGift);
        textBrandGift.setText(gift.getBrand());

        final TextView textNameGift = rowView.findViewById(R.id.textListNameGift);
        textNameGift.setText(gift.getName());

        final TextView textPriceGift = rowView.findViewById(R.id.textListPriceGift);
        textPriceGift.setText(String.valueOf(gift.getPrice()) + "€");

        final ImageView imageConfirmed = rowView.findViewById(R.id.imageConfirmed);
        if(gift.isConfirmed()) imageConfirmed.setVisibility(View.VISIBLE);

        if(!gift.isAvailable())
        {
            imageGift.setAlpha(0.3f);
            textBrandGift.setAlpha(0.3f);
            textNameGift.setAlpha(0.3f);
            textPriceGift.setAlpha(0.3f);
            imageConfirmed.setAlpha(0.3f);
        }

        return rowView;
    }
}