package alternance.miar.lp.iut.assistant_perenoel.controller;

import android.support.v7.app.AppCompatActivity;
import android.widget.Spinner;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.model.Children;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'appliquer des filtres pour la liste des enfants
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class FilterChildren extends Filter
{
    public FilterChildren(AppCompatActivity activity, ArrayList<Model> models)
    {
        super(activity, models);
    }

    @Override
    protected void applyFiltersEditText()
    {

    }

    @Override
    protected void applyFiltersSpinners()
    {
        for(Spinner spinnerMin : this.spinners.keySet())
        {
            for(Spinner spinnerMax : this.spinners.get(spinnerMin).keySet())
            {
                String attribut = this.spinners.get(spinnerMin).get(spinnerMax);
                int max = Integer.valueOf((String)spinnerMax.getSelectedItem());
                int min = Integer.valueOf((String)spinnerMin.getSelectedItem());

                this.filtersModels(attribut, min, max);
            }
        }
    }

    @Override
    protected void applyFiltersToggleButtons()
    {
        for(MultiStateToggleButton toggleButton : this.toggleButtons.keySet())
        {
            String attribut = this.toggleButtons.get(toggleButton);

            if(attribut.equals("getSex"))
            {
                if(toggleButton.getStates()[0])
                {
                    this.<String>filtersModels(attribut, "GARCON");
                }
                else if(toggleButton.getStates()[1])
                {
                    this.<String>filtersModels(attribut, "FILLE");
                }
            }
            else
            {
                if(toggleButton.getStates()[0])
                {
                    this.<Boolean>filtersModels(attribut, true);
                }
                else if(toggleButton.getStates()[1])
                {
                    this.<Boolean>filtersModels(attribut, false);
                }
            }
        }
    }

    @Override
    protected void initializeFiltersToggleButtons()
    {
        MultiStateToggleButton multiStateToggleButton;
        List<String> filters;

        filters = Arrays.asList("Oui", "Non");

        multiStateToggleButton = (MultiStateToggleButton)this.activity.findViewById(R.id.toggleButtonFilterSage);
        this.populateToggleButton(multiStateToggleButton, filters);
        this.toggleButtons.put(multiStateToggleButton,"isSage");

        multiStateToggleButton = (MultiStateToggleButton)this.activity.findViewById(R.id.toggleButtonFilterLetter);
        this.populateToggleButton(multiStateToggleButton, filters);
        this.toggleButtons.put(multiStateToggleButton,"isReceivedLetter");

        multiStateToggleButton = (MultiStateToggleButton)this.activity.findViewById(R.id.toggleButtonFilterGift);
        this.populateToggleButton(multiStateToggleButton, filters);
        this.toggleButtons.put(multiStateToggleButton,"isDeliveredGifts");

        filters = Arrays.asList("Garçon", "Fille");

        multiStateToggleButton = (MultiStateToggleButton)this.activity.findViewById(R.id.toggleButtonFilterSex);
        this.populateToggleButton(multiStateToggleButton, filters);
        this.toggleButtons.put(multiStateToggleButton,"getSex");
    }

    @Override
    protected void initializeFiltersSpinners()
    {
        Spinner spinner;
        List<String> filters;
        Map<Spinner, String> spinnerString;

        filters = this.getListSpinner(Children.COMPARATOR_CHILDREN_AGE, "getYears");

        spinner = this.activity.findViewById(R.id.spinnerFilterAgeMax);
        this.populateSpinner(spinner, filters, filters.size()-1);

        spinnerString = new HashMap<>();
        spinnerString.put(spinner, "getYears");

        spinner = this.activity.findViewById(R.id.spinnerFilterAgeMin);
        this.populateSpinner(spinner, filters, 0);

        this.spinners.put(spinner, spinnerString);

        filters = this.getListSpinner(Children.COMPARATOR_CHILDREN_CONFIRMED_GIFT, "sizeConfirmedGift");

        spinner = this.activity.findViewById(R.id.spinnerConfirmedGiftsMax);
        this.populateSpinner(spinner, filters, filters.size()-1);

        spinnerString = new HashMap<>();
        spinnerString.put(spinner, "sizeConfirmedGift");

        spinner = this.activity.findViewById(R.id.spinnerConfirmedGiftsMin);
        this.populateSpinner(spinner, filters, 0);

        this.spinners.put(spinner, spinnerString);
    }

    @Override
    protected void initializeFiltersEditText()
    {

    }
}
