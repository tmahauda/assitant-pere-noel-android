package alternance.miar.lp.iut.assistant_perenoel.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

/**
 * Classe qui permet de représenter un cadeau
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class Gift extends Model
{
    private String brand;
    private String name;
    private String description;
    private String urlImage;
    private double price;
    private boolean available;
    private boolean confirmed;

    public Gift(Parcel in)
    {
        this.id = in.readString();
        this.brand = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.urlImage = in.readString();
        this.price = in.readDouble();
        this.available = in.readByte() != 0;
        this.confirmed = in.readByte() != 0;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.id);
        dest.writeString(this.brand);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.urlImage);
        dest.writeDouble(this.price);
        dest.writeByte((byte) (this.available ? 1 : 0));
        dest.writeByte((byte) (this.confirmed ? 1 : 0));
    }

    public static final Parcelable.Creator<Gift> CREATOR = new Parcelable.Creator<Gift>()
    {
        public Gift createFromParcel(Parcel in)
        {
            return new Gift(in);
        }

        public Gift[] newArray(int size)
        {
            return new Gift[size];
        }
    };

    public Gift(String id, String brand, String name, String description, String urlImage, double price, boolean available, boolean confirmed)
    {
        this.id = id;
        this.brand = brand;
        this.name = name;
        this.description = description;
        this.urlImage = urlImage;
        this.price = price;
        this.available = available;
        this.confirmed = confirmed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    @Override
    public String toString() {
        return "Gift{" +
                "id='" + id + '\'' +
                ", brand='" + brand + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", urlImage='" + urlImage + '\'' +
                ", price=" + price +
                ", available=" + available +
                ", confirmed=" + confirmed +
                '}';
    }

    public static final Comparator<Model> COMPARATOR_GIFT_BRAND = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Gift gift1 = (Gift)model1;
            Gift gift2 = (Gift)model2;

            return gift1.getBrand().compareTo(gift2.getBrand());
        }
    };

    public static final Comparator<Model> COMPARATOR_GIFT_NAME = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Gift gift1 = (Gift)model1;
            Gift gift2 = (Gift)model2;

            return gift1.getName().compareTo(gift2.getName());
        }
    };

    public static final Comparator<Model> COMPARATOR_GIFT_PRICE = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Gift gift1 = (Gift)model1;
            Gift gift2 = (Gift)model2;

            if(gift1.getPrice() > gift2.getPrice())
            {
                return 1;
            }
            else if(gift1.getPrice() < gift2.getPrice())
            {
                return -1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_GIFT_AVAILABLE = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Gift gift1 = (Gift)model1;
            Gift gift2 = (Gift)model2;

            if(gift1.isAvailable() && !gift2.isAvailable())
            {
                return -1;
            }
            else if(!gift1.isAvailable() && gift2.isAvailable())
            {
                return 1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_GIFT_CONFIRMED = new Comparator<Model>()
    {
        @Override
        public int compare(Model model1, Model model2)
        {
            Gift gift1 = (Gift)model1;
            Gift gift2 = (Gift)model2;

            if(gift1.isConfirmed() && !gift2.isConfirmed())
            {
                return 1;
            }
            else if(!gift1.isConfirmed() && gift2.isConfirmed())
            {
                return -1;
            }
            else return 0;
        }
    };

    public static final Comparator<Model> COMPARATOR_GIFT_DEFAULT = new Comparator<Model>()
    {
        final Comparator<Model>[] comparators = new Comparator[]{COMPARATOR_GIFT_CONFIRMED, COMPARATOR_GIFT_AVAILABLE,
                COMPARATOR_GIFT_PRICE, COMPARATOR_GIFT_BRAND, COMPARATOR_GIFT_NAME};

        @Override
        public int compare(Model model1, Model model2)
        {
            return COMPARATOR_MODEL(comparators).compare(model1, model2);
        }
    };
}
