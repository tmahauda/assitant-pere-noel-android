package alternance.miar.lp.iut.assistant_perenoel.model;

import android.os.Parcelable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Objects;

/**
 * Classe qui permet de représenter un modèle
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public abstract class Model implements Parcelable
{
    protected String id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Model model = (Model) o;
        return Objects.equals(id, model.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public <T> T get(String attribute)
    {
        Class<?> clazz = this.getClass();

        try
        {
            Method method = clazz.getMethod(attribute, (Class<?>[]) null);
            return (T)method.invoke(this, (Object[]) null);
        }
        //catch getMethod
        catch (NoSuchMethodException | SecurityException e)
        {
            return null;
        }
        //catch invoke
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
        {
            return null;
        }
    }

    public static Comparator<Model> COMPARATOR_MODEL(final Comparator<Model>[] comparators)
    {
        return new Comparator<Model>()
        {
            @Override
            public int compare(Model model1, Model model2)
            {
                if(comparators.length > 0)
                {
                    if(comparators.length > 1)
                    {
                        for(int i=0; i<comparators.length - 1; i++)
                        {
                            int comp = comparators[i].compare(model1, model2);

                            if(comp != 0)
                            {
                                return comp;
                            }
                        }
                    }

                    return comparators[comparators.length-1].compare(model1, model2);
                }
                else return 0;
            }
        };
    }
}
