package alternance.miar.lp.iut.assistant_perenoel.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import alternance.miar.lp.iut.assistant_perenoel.R;
import alternance.miar.lp.iut.assistant_perenoel.controller.Filter;
import alternance.miar.lp.iut.assistant_perenoel.controller.FilterGift;
import alternance.miar.lp.iut.assistant_perenoel.model.Model;

/**
 * Classe qui permet d'afficher les filtres à appliquer pour la liste des cadeaux
 *
 * @author MAHAUDA Théo, PINEAU Mathieu (Janvier 2019)
 * @version 1.0
 */
public class FiltersGift extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters_gift);

        final ArrayList<Model> models = getIntent().getExtras().getParcelableArrayList("gifts");

        //Filtre
        final Filter filter = new FilterGift(this, models);
        filter.initializeFilters();

        //Enregistrer
        final Button buttonRecordFilters = findViewById(R.id.buttonRecordFiltersGifts);
        buttonRecordFilters.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.putParcelableArrayListExtra("gifts", filter.getModels());
                setResult(Letter.RESULT_OK, intent);
                finish();
            }
        });
    }
}
